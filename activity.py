"""
1. create a list of any 5 names called 'students'
2. create a list of any 5 grades for the students
3. use a loop to iterate through both list the following format
"""
name_list = [ "Dipsy", "Laa Laa", "Tinky Winky", "Po", "Sun Baby" ]
grades = [ 100, 98, 92, 99, 87 ]
index = 0
for i in name_list:
	print(f"the grade of {name_list[index]} is {grades[index]} ")
	index = index + 1

"""

-------
1. create a car dictionary with the following keys: brand, model, year of make, color (any values)
2. print the following statement from the details. "I own a <color><brand><mode> and it was made in <year of make>"
"""

car = {
	"brand": "Land Rover",
	"model": "Discovery",
	"color": "Narvik Black",
	"year": "2022"

}

print(f'I own a {car["color"]} {car["brand"]} {car["model"]} and it was made in {car["year"]}.')

"""
_______

1. create a function that gets a square of a number (multiply by itself)
"""

number = (int(input('Enter a number to get its square: ')))
def square(num):
    return num*num

print("Square of ",number, "is ", square(number))


"""
_______
1. create a function that takes one of the following languages as its parameter
a. French
b. SPanish
c. Japanese

2. depending on which language is given, the function prints "Hello World!" in that language
3. Add a condition that asks the user to input a valid language if the given parameter does not match any of the above

"""


def translate(language) : 
    if language == "French" or language == "french" :
        print("Bonjour le monde !")
    elif language == "Spanish" or language == "spanish" :
        print("¡Hola mundo!")
    elif language == "Japanese" or language == "japanese" :
        print("Kon'nichiwa sekai")
    elif language == "Filipino" or language == "filipino" :
        print("Mabuhay!")
    else :
        print("We only translate French, Spanish, Filipino and Japanese.")

translate(input("Enter a language - Must be French, Spanish, Filipino, Or Japanese only: "))

